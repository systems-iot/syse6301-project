import lib.import_sumo

import traci
import traci.constants as tc
from time import sleep
import random
from generate_traffic import generate_traffic

from iotery_embedded_python_sdk import Iotery

# Set up and start SUMO
sumoBinary = "sumo-gui"
sumoCmd = [
    sumoBinary,
    "-c",
    "utd.sumocfg",
    "--start",
    "--time-to-teleport",
    "100000000",
]
traci.start(sumoCmd)
step = 0


TEAM_ID = "your-team-id"  # team ID found on the dashboard: https://iotery.io/system

# Sample Widget Delivery Vehicle with a trip (a trip is an "auto-route")
traci.route.add("delivery_trip_1", ["GATE_1_IN", "DELIVERY_DROP"])
traci.vehicle.add("delivery_veh_1", "delivery_trip_1")
traci.vehicle.setColor("delivery_veh_1", [255, 0, 0])

# You can also add a delivery vehicle with a specific route:
traci.route.add("delivery_route_1", ["GATE_1_IN", "-gneE5", "GATE_2_OUT"])
traci.vehicle.add("delivery_veh_2", "delivery_route_1")

# You will need to create the appropriate devices in your team's Iotery account.
# Each device should correspond to a vehicle (or a traffic light)
# Remember, you can only use the embedded device SDK in this file!

# delivery_vehicle_connector = Iotery()
# delivery_vehicle_token = delivery_vehicle_connector.getDeviceTokenBasic(data={"key": "your-vehicle-key",
#                                                                               "serial": "your-vehicle-serial", "secret": "your-vehicle-secret", "teamUuid": TEAM_ID})
# delivery_vehicle_connector.set_token(d["token"])
# delivery_vehicle_connector = iotery.getMe()

# Helpful hint: include a dispatch controller device (a general sensor that sits at your manufacturing plant) and keeps tabs on your devices that are at your HQ at a given time.
# This can be used communicate with your VAS' (by posting data in-the-loop) and using webhooks to post to your VAS


# To control a traffic light, you will need to find its ID using netedit.  For example, the last intersection traffic light that a vehicle will approach coming from the manufacturing HQ would be 81742684:
# This will set the phase of all lights (going clockwise around the intersection) to red.
traci.trafficlight.setRedYellowGreenState("81742684", "rrrrrrrr")
# Each "r" corresponds to a light for a lane.  It may look like there are only 6 total lanes in the intersection, but don't forget the turn lanes (so there are actually 8) controlled lights in the intersection!
# So, to make only the northern most lights green, the string would be "rrrrgggg"
# More information: http://sumo.sourceforge.net/pydoc/traci._trafficlight.html

while step < 10000:

    # Do your stuff here with your vehicles, dispatch controller, and traffic lights!

    # --- DO NOT MODIFY ---
    gen_trip_name, gen_vehicle, gen_trip = generate_traffic(step)
    traci.route.add(gen_trip_name, gen_trip)
    traci.vehicle.add(gen_vehicle, gen_trip_name)
    traci.simulationStep()
    step += 1
    sleep(0.01)
    # --- DO NOT MODIFY ---


traci.close()
